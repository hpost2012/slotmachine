/* jshint esversion:6 */

let imgDirectory = "images/";

let imgList = [];
let Random;
for (let count = 0; count < 10; count++){
    imgList[count] = imgDirectory + count + ".jpeg";
    new Image().src = imgList[count];
}
document.getElementById("betAmount").value = "1";

let betAmount = 5;
let amountLeft = 100;
let totalWon = 0;

let counter = 0;

let row1 = [];
let row2 = [];
let row3 = [];
document.getElementById("betAmount").value = betAmount;
/*
    
*/
function slotFunction() {

    betAmount = document.getElementById("betAmount").value;
    betAmount = Math.floor(betAmount);
    document.getElementById("betAmount").value = betAmount;
    let textArea = document.getElementById("hidden");
    if (betAmount > 100){
        textArea.style.top="-100px";
        textArea.style.fontSize= "30px";
        textArea.style.backgroundColor="Yellow";
        textArea.innerText = "You might be feeling lucky! But we only accept a max bet of $100.00";
        document.getElementById("betAmount").value = "5";
        document.getElementById("startBTN").disabled = true;
        blink(350, 15);
    }else {

        if (amountLeft < betAmount && amountLeft > 0) {
            betAmount = amountLeft;
            amountLeft = amountLeft - betAmount;
            document.getElementById("amountWon").innerText = " ";
            
            document.getElementById("hidden").visibility = "hidden";
            row1.length = 0;
            row2.length = 0;
            row3.length = 0;
            totalWon = 0;
            
            document.getElementById("money").innerText= "$" + amountLeft + ".00";
            Random = setInterval(timer, 100);
        }else if (amountLeft === 0){
            let outofMoney = "true";
            textArea.style.top="-100px";
            textArea.style.fontSize= "30px";
            textArea.style.backgroundColor="Yellow";
            textArea.innerText = "You ran out of money! Don't worry just refresh page to continue!";
            document.getElementById("betAmount").value = "5";
            document.getElementById("startBTN").disabled = true;
            blink(350, 15, outofMoney);
            document.getElementById("startBTN").disabled = true;
        }else {
            
            amountLeft = amountLeft - betAmount;
            document.getElementById("amountWon").innerText = " ";
            
            document.getElementById("hidden").visibility = "hidden";
            row1.length = 0;
            row2.length = 0;
            row3.length = 0;
            totalWon = 0;
            
            document.getElementById("money").innerText= "$" + amountLeft + ".00";
            Random = setInterval(timer, 100);
        }
    }
}    

function timer() {
    counter++;
        let leftnum1 = randomNumGen();
        let midnum1= randomNumGen();
        let rightnum1 = randomNumGen();
        let leftnum2 = randomNumGen();
        let midnum2= randomNumGen();
        let rightnum2 = randomNumGen();
        let leftnum3 = randomNumGen();
        let midnum3= randomNumGen();
        let rightnum3 = randomNumGen();
       
        document.leftImg1.src = imgList[leftnum1];
        document.centerImg1.src = imgList[midnum1];
        document.rightImg1.src = imgList[rightnum1];
        document.leftImg2.src = imgList[leftnum2];
        document.centerImg2.src = imgList[midnum2];
        document.rightImg2.src = imgList[rightnum2];
        document.leftImg3.src = imgList[leftnum3];
        document.centerImg3.src = imgList[midnum3];
        document.rightImg3.src = imgList[rightnum3];

        if (counter >10) {
            row1.push(imgList[leftnum1]);3
            row1.push(imgList[midnum1]);
            row1.push(imgList[rightnum1]);
            row2.push(imgList[leftnum2]);
            row2.push(imgList[midnum2]);
            row2.push(imgList[rightnum2]);
            row3.push(imgList[leftnum3]);
            row3.push(imgList[midnum3]);
            row3.push(imgList[rightnum3]);
           
            findWinners();
            counter = 0;

            clearInterval(Random);
        }
}

function randomNumGen(){
    return Math.floor(Math.random() *10);
}

function findWinners(){
    let totalWinners = 0;
    let wild = "images/3.jpeg";
    let wildLoc1 = null;
    let wildLoc2 = null;
    let wildLoc3 = null;
    //Check Winners Row 1
    if ((row1[0]===row1[1]) && (row1[0]===row1[2])) {
        totalWinners += 3;
    }
    if (row1[0]===row1[1]) {
        totalWinners += 2;
    }

    //Check Wild Winners Row 1
    if (row1.includes(wild)) {
        wildLoc1 = row1.indexOf(wild);
    }
    if ((wildLoc1 === 2 && row1[0]===row1[1]) || (wildLoc1 === 0 && row1[1]===row1[2]) || (wildLoc1===1 && row1[0]===row1[2])){
        totalWinners += 3;
    }else if ((wildLoc1 ===0 && row1[1]===wild) || (wildLoc1 ===1 && row1[2]===wild) || (wildLoc1===0 && row1[2]===wild)){
        totalWinners += 3;
    }else if (wildLoc1===0 || wildLoc1===1){
        
        totalWinners += 2;
    }

    //Check Winners Row 2
    if ((row2[0]===row2[1]) && (row2[0]===row2[2])) {
        totalWinners += 3;
    }
    if (row2[0]===row2[1]) {
        totalWinners += 2;
    }

    //Check Wild Winners Row 2
    if (row2.includes(wild)) {
        wildLoc2 = row2.indexOf(wild);
    }
    if ((wildLoc2 === 2 && row2[0]===row2[1]) || (wildLoc2 === 0 && row2[1]===row2[2]) || (wildLoc2===1 && row2[0]===row2[2])){
        totalWinners += 3;
    }else if ((wildLoc2 ===0 && row2[1]===wild) || (wildLoc2 ===1 && row2[2]===wild) || (wildLoc2===0 && row2[2]===wild)){
        totalWinners += 3;
    }else if (wildLoc2===0 || wildLoc2===1){
        
        totalWinners += 2;
    }

    //Check Winners Row 3
    if ((row3[0]===row3[1]) && (row3[0]===row3[2])) {
        totalWinners += 3;
    }
    if (row3[0]===row3[1]) {
        totalWinners += 2;
    }

    //Check Wild Winners Row 3
    if (row3.includes(wild)) {
        wildLoc3 = row3.indexOf(wild);
    }
    if ((wildLoc3 === 2 && row3[0]===row3[1]) || (wildLoc3 === 0 && row3[1]===row3[2]) || (wildLoc3===1 && row3[0]===row3[2])){
        totalWinners += 3;
    }else if ((wildLoc3 ===0 && row3[1]===wild) || (wildLoc3 ===1 && row3[2]===wild) || (wildLoc3===0 && row3[2]===wild)){
        totalWinners += 3;
    }else if (wildLoc3===0 || wildLoc3===1){
        
        totalWinners += 2;
    }
    
    if (totalWinners > 0){
       
        if (totalWinners >= 6) {
            document.getElementById("hidden").innerText = "BIG WIN!!!";
            document.getElementById("startBTN").disabled = true;
            blink(200, 10);
        }else {
            document.getElementById("startBTN").disabled = true;
            blink(200, 10);
        }
    }

    totalWon = betAmount * totalWinners;
    amountLeft = amountLeft + totalWon;
    document.getElementById("amountWon").innerText = "$" + totalWon + ".00";
    document.getElementById("money").innerText = "$" + amountLeft + ".00";
   
}
function blink(blinkSpeed, blinkLength, outofMoney) {
    let elem = document.getElementById("hidden");
    let pos = 0;
    
    let id = setInterval(frame, blinkSpeed);
    function frame() {
        if (pos === blinkLength) {
            clearInterval(id);
            if (outofMoney != "true"){

                document.getElementById("startBTN").disabled = false;
                document.getElementById("hidden").innerText = "YOU WIN!!!";
            document.getElementById("hidden").style.backgroundColor = "white";
            document.getElementById("hidden").style.top = "-50px";
            elem.style.visibility = "hidden";
            }

            
        } else {
            pos++;
           
            if (elem.style.visibility==="hidden"){
                elem.style.visibility="visible";
            }else{
                elem.style.visibility="hidden";
            }
            
        }
    }
}
function dispFireworks() {
    $('.display').fireworks({
    
        sound:false,// sound effect
      
        opacity: 0.9,
      
        width:'1500px',
      
        height:'200px'
      
      });
}